import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ProductModel } from '../../../../core/models/products.model';

@Component({
  selector: 'app-products-item',
  templateUrl: './products-item.component.html',
  styleUrls: ['./products-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsItemComponent {
  @Input() public product: ProductModel;
}
