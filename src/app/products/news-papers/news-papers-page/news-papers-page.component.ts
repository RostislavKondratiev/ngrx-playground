import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { NewsPapersLoad } from '../../store/actions/news-papers.actions';
import { ProductState } from '../../store/reducers/products.reducer';

@Component({
  selector: 'app-news-papers-page',
  templateUrl: './news-papers-page.component.html',
  styleUrls: ['./news-papers-page.component.scss']
})
export class NewsPapersPageComponent implements OnInit {
  constructor(private store: Store<ProductState>) {

  }

  public ngOnInit() {
    this.store.dispatch(new NewsPapersLoad());
  }
}
