import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductState } from './store/reducers/products.reducer';
import { NotificationService } from '../ui/notification/notification.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  constructor(private store: Store<ProductState>, private notification: NotificationService) {
  }

  public ngOnInit() {
  }

  public openNotification() {
    this.notification.openNotification({title: 'Test', message: 'Some Message', duration: 10000});
  }
}
