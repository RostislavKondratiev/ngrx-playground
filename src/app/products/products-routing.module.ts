import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products.component';
import { BooksPageComponent } from './books/books-page/books-page.component';
import { MagazinesPageComponent } from './magazines/magazines-page/magazines-page.component';
import { BookDetailsPageComponent } from './books/book-details-page/book-details-page.component';
import { MagazineDetailsPageComponent } from './magazines/magazine-details-page/magazine-details-page.component';
import { NewsPapersPageComponent } from './news-papers/news-papers-page/news-papers-page.component';
import { PostersPageComponent } from './posters/posters-page/posters-page.component';

const ROUTES: Routes = [
  {path: '', component: ProductsComponent},
  {
    path: 'books', component: BooksPageComponent, children: [
      {path: ':id', component: BookDetailsPageComponent}
    ]
  },
  {
    path: 'magazines', component: MagazinesPageComponent, children: [
      {path: ':id', component: MagazineDetailsPageComponent}
    ]
  },
  {path: 'news-papers', component: NewsPapersPageComponent},
  {path: 'posters', component: PostersPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {

}
