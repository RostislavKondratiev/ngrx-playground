import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductState } from '../../store/reducers/products.reducer';
import { BooksLoad } from '../../store/actions';
import * as fromStore from '../../store';
import { Observable } from 'rxjs';
import { ProductModel } from '../../../core/models/products.model';
import { Dictionary } from '@ngrx/entity/src/models';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-books-page',
  templateUrl: './books-page.component.html',
  styleUrls: ['./books-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooksPageComponent implements OnInit {
  public books$: Observable<any>;

  constructor(private store: Store<ProductState>) {
  }

  public ngOnInit() {
    this.store.dispatch(new BooksLoad());
    this.books$ = this.store.select(fromStore.getBooksEntities).pipe(
      map((value) => Object.values(value))
    );
  }
}
