import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from './../../store';

@Component({
  selector: 'app-magazines-page',
  templateUrl: './magazines-page.component.html',
  styleUrls: ['./magazines-page.component.scss']
})
export class MagazinesPageComponent implements OnInit {
  constructor(private store: Store<fromStore.ProductsState>) {
  }

  public ngOnInit() {
    this.store.select(fromStore.getMagazinesEntities).subscribe((v) => console.log(v));
    this.store.dispatch(new fromStore.MagazinesLoad());
  }
}
