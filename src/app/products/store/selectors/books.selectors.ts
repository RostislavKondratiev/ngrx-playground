import { productAdapter, selectProductsState } from '../reducers/products.reducer';
import { ProductsState } from '../reducers';
import { createSelector } from '@ngrx/store';
import * as selectors from './products.selectors';

export const selectBooksState = createSelector(
  selectProductsState,
  (state: ProductsState) => {
    return state.books;
  }
);

export const getBooksEntities = createSelector(
  selectBooksState,
  selectors.selectEntities
);

export const getBooksIds = createSelector(
  selectBooksState,
  selectors.selectIds
);
