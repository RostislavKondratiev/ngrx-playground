import { createSelector } from '@ngrx/store';
import { selectProductsState } from '../reducers/products.reducer';
import { ProductsState } from '../reducers';
import { postersAdapter } from '../reducers/posters.reducer';

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = postersAdapter.getSelectors();

export const selectPostersState = createSelector(
  selectProductsState,
  (state: ProductsState) => state.posters
);

export const getPostersEntities = createSelector(
  selectPostersState,
  selectEntities
);

export const getPostersMeta = createSelector(
  selectPostersState,
  (state) => state.meta
);

export const getPostersLoading = createSelector(
  selectPostersState,
  (state) => state.loading
);
