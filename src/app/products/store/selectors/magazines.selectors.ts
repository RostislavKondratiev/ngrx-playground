import { createSelector } from '@ngrx/store';
import { selectProductsState } from '../reducers/products.reducer';
import { ProductsState } from '../reducers';
import * as selectors from './products.selectors';


export const selectMagazinesState = createSelector(
  selectProductsState,
  (state: ProductsState) => state.magazines
);

export const getMagazinesEntities = createSelector(
  selectMagazinesState,
  selectors.selectEntities
);

export const getMagazinesIds = createSelector(
  selectMagazinesState,
  selectors.selectIds
);

