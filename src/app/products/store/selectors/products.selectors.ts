import { productAdapter } from '../reducers/products.reducer';

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = productAdapter.getSelectors();
