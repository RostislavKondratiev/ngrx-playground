import { ProductsLoad, ProductsLoadFail, ProductsLoadSuccess } from './products.actions';

export enum BooksTypes {
  LOAD = '[Books] LOAD',
  LOAD_SUCCESS = '[Books] LOAD SUCCESS',
  LOAD_FAIL = '[Books] LOAD FAIL'
}

export class BooksLoad extends ProductsLoad {
  public type = BooksTypes.LOAD;
}

export class BooksLoadSuccess extends ProductsLoadSuccess {
  public type = BooksTypes.LOAD_SUCCESS;
}

export class BooksLoadFail extends ProductsLoadFail {
  public type = BooksTypes.LOAD_FAIL;
}

export type BookActions
  = BooksLoad
  | BooksLoadSuccess
  | BooksLoadFail;
