import { TableLoad, TableLoadFail, TableLoadSuccess } from '../../../ui/table/table-store/table.actions';

export enum PostersTypes {
  LOAD = '[Posters] Load',
  LOAD_SUCCESS = '[Posters] Load Success',
  LOAD_FAIL = '[Posters] Load Fail'
}

export class PostersLoad extends TableLoad {
  public type = PostersTypes.LOAD;
}

export class PostersLoadSuccess<T> extends TableLoadSuccess<T> {
  public type = PostersTypes.LOAD_SUCCESS;
}

export class PostersLoadFail extends TableLoadFail {
  public type = PostersTypes.LOAD_FAIL;
}
