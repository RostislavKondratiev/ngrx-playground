import { ProductsLoad, ProductsLoadFail, ProductsLoadSuccess } from './products.actions';

export enum NewsPapersTypes {
  LOAD = '[News] LOAD',
  LOAD_SUCCESS = '[News] LOAD SUCCESS',
  LOAD_FAIL = '[News] LOAD FAIL'
}

export class NewsPapersLoad extends ProductsLoad {
  public type = NewsPapersTypes.LOAD;
}

export class NewsPapersLoadSuccess extends ProductsLoadSuccess {
  public type = NewsPapersTypes.LOAD_SUCCESS;
}

export class NewsPapersLoadFail extends ProductsLoadFail {
  public type = NewsPapersTypes.LOAD_FAIL;
}
