import { Action } from '@ngrx/store';
import { ProductModel } from '../../../core/models/products.model';

export enum ProductsTypes {
  LOAD = 'LOAD',
  LOAD_SUCCESS = 'LOAD SUCCESS',
  LOAD_FAIL = 'LOAD FAIL'
}

export abstract class ProductsLoad implements Action {
  public type;

  constructor() {
  }
}

export abstract class ProductsLoadSuccess implements Action {
  public type;

  constructor(public payload: ProductModel[]) {
  }
}

export abstract class ProductsLoadFail implements Action {
  public type;

  constructor(public payload: any) {
  }
}

export type ProductsActions
  = ProductsLoad
  | ProductsLoadSuccess
  | ProductsLoadFail;
