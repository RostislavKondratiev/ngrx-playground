import { ProductsLoad, ProductsLoadFail, ProductsLoadSuccess } from './products.actions';

export enum MagazinesTypes {
  LOAD = '[Magazines] LOAD',
  LOAD_SUCCESS = '[Magazines] LOAD SUCCESS',
  LOAD_FAIL = '[Magazines] LOAD FAIL'
}

export class MagazinesLoad extends ProductsLoad {
  public type = MagazinesTypes.LOAD;
}

export class MagazinesLoadSuccess extends ProductsLoadSuccess {
  public type = MagazinesTypes.LOAD_SUCCESS;
}

export class MagazinesLoadFail extends ProductsLoadFail {
  public type = MagazinesTypes.LOAD_FAIL;
}

export type MagazinesActions
  = MagazinesLoad
  | MagazinesLoadSuccess
  | MagazinesLoadFail;

