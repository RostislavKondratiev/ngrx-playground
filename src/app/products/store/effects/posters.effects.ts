import { Injectable } from '@angular/core';
import { TableEffects } from '../../../ui/table/table-store/table.effects';
import { Actions, Effect } from '@ngrx/effects';
import { PostersHttpService } from '../../../core/http-services/posters-http.service';
import * as actions from './../actions';

@Injectable()
export class PostersEffects extends TableEffects {
  constructor(actions$: Actions, private http: PostersHttpService) {
    super(actions$);
  }

  @Effect() getPosters = this.fetch(
    actions.PostersTypes.LOAD,
    this.http.getPosters.bind(this.http),
    {success: actions.PostersLoadSuccess, fail: actions.PostersLoadFail}
  );
}
