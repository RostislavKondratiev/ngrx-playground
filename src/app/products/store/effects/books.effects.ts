import { Injectable } from '@angular/core';
import { ProductsEffects } from './products.effects';
import { Actions, Effect } from '@ngrx/effects';
import { ProductsHttpService } from '../../../core/http-services/products-http.service';
import * as actions from './../actions';

@Injectable()
export class BooksEffects extends ProductsEffects {
  constructor(actions$: Actions,
              private http: ProductsHttpService) {
    super(actions$);
  }

  @Effect() fetchBooks$ = this.fetch(
    actions.BooksTypes.LOAD,
    this.http.getBooks.bind(this.http),
    {success: actions.BooksLoadSuccess, fail: actions.BooksLoadFail}
  );
}
