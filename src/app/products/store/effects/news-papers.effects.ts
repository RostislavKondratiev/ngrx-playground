import { Injectable } from '@angular/core';
import { ProductsEffects } from './products.effects';
import { Actions, Effect } from '@ngrx/effects';
import { ProductsHttpService } from '../../../core/http-services/products-http.service';
import * as actions from './../actions';
import { NewsPapersLoadFail, NewsPapersLoadSuccess } from './../actions';

@Injectable()
export class NewsPapersEffects extends ProductsEffects {
  constructor(actions$: Actions,
              private http: ProductsHttpService) {
    super(actions$);
  }

  @Effect() fetchNewsPapers = this.fetch(
    actions.NewsPapersTypes.LOAD,
    this.http.getNewsPapers.bind(this.http),
    {success: NewsPapersLoadSuccess, fail: NewsPapersLoadFail}
  );
}
