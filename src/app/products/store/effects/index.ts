import { BooksEffects } from './books.effects';
import { MagazinesEffects } from './magazines.effects';
import { NewsPapersEffects } from './news-papers.effects';
import { PostersEffects } from './posters.effects';

export const effects = [BooksEffects, MagazinesEffects, NewsPapersEffects, PostersEffects];
