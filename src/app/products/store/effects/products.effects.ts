import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export abstract class ProductsEffects {

  constructor(protected actions$: Actions) {
  }

  protected fetch(type: string, request: () => Observable<any>, target: { success: any, fail: any }) {
    return this.actions$
      .pipe(
        ofType(type),
        switchMap(() => request()
          .pipe(
            map((res) => new target.success(res)),
            catchError((err) => new target.fail(err))
          )
        ),
      );
  }
}
