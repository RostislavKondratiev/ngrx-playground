import { Injectable } from '@angular/core';
import { ProductsEffects } from './products.effects';
import { Actions, Effect } from '@ngrx/effects';
import { ProductsHttpService } from '../../../core/http-services/products-http.service';
import * as actions from './../actions';

@Injectable()
export class MagazinesEffects extends ProductsEffects {
  constructor(actions$: Actions,
              private http: ProductsHttpService) {
    super(actions$);
  }

  @Effect() fetchMagazines$ = this.fetch(
    actions.MagazinesTypes.LOAD,
    this.http.getMagazines.bind(this.http),
    {success: actions.MagazinesLoadSuccess, fail: actions.MagazinesLoadFail}
  );
}
