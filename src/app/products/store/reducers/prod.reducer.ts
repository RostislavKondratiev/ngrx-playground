import { productAdapter, ProductState } from './products.reducer';

export abstract class ProdReducer {

  load(state, action) {
    return {...state, loading: true};
  }

  loadSuccess(state: ProductState, action: any) {
    return productAdapter.addAll(action.payload, {...state, loading: false, loaded: true});
  }

  loadFail(state: ProductState, action: any) {
    return {
      ...state,
      loading: false,
      error: action.payload
    };
  }
}

