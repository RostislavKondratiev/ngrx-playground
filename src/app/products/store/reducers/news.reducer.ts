import { Action, createReducer, Store } from 'ngrx-actions';
import { INITIAL_STATE, ProductState } from './products.reducer';
import { ProdReducer } from './prod.reducer';
import { NewsPapersLoad, NewsPapersLoadFail, NewsPapersLoadSuccess } from '../actions/news-papers.actions';

@Store(INITIAL_STATE)
export class NewsReducer extends ProdReducer {
  @Action(NewsPapersLoad)
  load(state: ProductState, action) {
    return super.load(state, action);
  }

  @Action(NewsPapersLoadSuccess)
  loadSuccess(state: ProductState, action) {
    return super.loadSuccess(state, action);
  }

  @Action(NewsPapersLoadFail)
  loadFail(state: ProductState, action) {
    return super.loadFail(state, action);
  }
}

export function newsReducer(state: ProductState, action): (state: ProductState, action) => ProductState {
  return createReducer(NewsReducer)(state, action);
}
