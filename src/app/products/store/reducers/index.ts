import { ActionReducerMap, combineReducers, compose } from '@ngrx/store';
import { productReducer, ProductState, testReducer } from './products.reducer';
import { newsReducer } from './news.reducer';
import { TableState } from '../../../ui/table/table-store/table.reducer';
import { PostersModel } from '../../../core/models/posters.model';
import { postersReducer } from './posters.reducer';

// export const reducers = combineReducers({
//   'books': productReducer('Books'),
//   'magazines': productReducer('Magazines')
// });

export interface ProductsState {
  books: ProductState;
  magazines: ProductState;
  news: ProductsState;
  posters: TableState<PostersModel>;
}

export const reducers: ActionReducerMap<any> = {
  books: productReducer('Books'),
  magazines: productReducer('Magazines'),
  news: newsReducer,
  posters: postersReducer
};
