import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ProductModel } from '../../../core/models/products.model';
import { ProductsTypes } from '../actions';
import { createFeatureSelector } from '@ngrx/store';
import { ProductsState } from './index';

export const productAdapter = createEntityAdapter<ProductModel>();

export interface ProductState extends EntityState<ProductModel> {
  loading: boolean;
  loaded: boolean;
  error: any;
}

export const INITIAL_STATE: ProductState = productAdapter.getInitialState({
  loading: false,
  loaded: false,
  error: null
});

export function productReducer(type: string): (state: ProductState, action: any) => ProductState {
  return (state: ProductState = INITIAL_STATE, action: any): ProductState => {
    switch (action.type) {
      case `[${type}] ${ProductsTypes.LOAD}`: {
        return {
          ...state,
          loading: true
        };
      }
      case `[${type}] ${ProductsTypes.LOAD_SUCCESS}`: {
        return productAdapter.addAll(action.payload, {...state, loading: false, loaded: true});
      }
      case `[${type}] ${ProductsTypes.LOAD_FAIL}` : {
        return {
          ...state,
          loading: false,
          error: action.payload
        };
      }
    }
    return state;
  };
}

export function testReducer(state: ProductsState, action) {
  switch (action.type) {
    case 'TEST': {
      console.log('test here');
      return state;
    }
  }
}

export const selectProductsState = createFeatureSelector<ProductsState>('products');
