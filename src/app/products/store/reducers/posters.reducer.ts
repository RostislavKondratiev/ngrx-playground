import { TableReducer, TableState } from '../../../ui/table/table-store/table.reducer';
import { PostersModel } from '../../../core/models/posters.model';
import { Action, createReducer, Store } from 'ngrx-actions';
import { PostersLoad, PostersLoadFail, PostersLoadSuccess } from '../actions';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { TableMeta } from '../../../ui/table/ds-meta';

export interface PostersState extends EntityState<PostersModel> {
  loading: boolean;
  error: null;
  meta: TableMeta | {};
}

export const postersAdapter = createEntityAdapter<PostersModel>();

const INITIAL_STATE: PostersState = postersAdapter.getInitialState({
  loading: false,
  error: null,
  meta: {}
});

@Store(INITIAL_STATE)
export class PostersReducer extends TableReducer<PostersModel> {
  constructor() {
    super(postersAdapter);
  }

  @Action(PostersLoad)
  load(state: TableState<PostersModel>, action) {
    return super.load(state, action);
  }

  @Action(PostersLoadSuccess)
  loadSuccess(state: TableState<PostersModel>, action: PostersLoadSuccess<PostersModel>) {
    return super.loadSuccess(state, action);
  }

  @Action(PostersLoadFail)
  loadFail(state: TableState<PostersModel>, action: PostersLoadFail) {
    return super.loadFail(state, action);
  }
}

export function postersReducer(state: TableState<PostersModel>, action): (state: TableState<PostersModel>, action) => TableState<PostersModel> {
  return createReducer(PostersReducer)(state, action);
}
