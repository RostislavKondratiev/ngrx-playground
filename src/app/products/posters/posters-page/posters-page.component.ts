import { Component, OnInit, ViewChild } from '@angular/core';
import { PostersDatabase } from './datasource/posters.database';
import { PostersDatasource } from './datasource/posters.datasource';
import { SortDirective } from '../../../ui/sort/sort.directive';
import { PostersResponse } from '../../../core/models/posters.model';
import { ProductsState } from '../../store/reducers';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-posters-page',
  templateUrl: './posters-page.component.html',
  styleUrls: ['./posters-page.component.scss'],
  providers: [PostersDatabase]
})
export class PostersPageComponent implements OnInit {
  public columns = ['id', 'title', 'price'];

  public ds: PostersDatasource;
  @ViewChild('sort') private sort: SortDirective;

  constructor(public db: PostersDatabase<PostersResponse>, private store: Store<ProductsState>) {

  }

  public ngOnInit() {
    this.ds = new PostersDatasource(this.db, this.sort, this.store);
  }
}
