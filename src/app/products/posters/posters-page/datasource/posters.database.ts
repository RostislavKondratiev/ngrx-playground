import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProductsState } from '../../../store/reducers';
import { Store } from '@ngrx/store';
import { PostersLoad } from '../../../store/actions';
import { TableDataBase } from '../../../../ui/table/ds-meta';

@Injectable()
export class PostersDatabase<T> implements TableDataBase<T> {

  constructor(private api: HttpClient, private store: Store<ProductsState>) {
  }

  public load(additionalParams) {
    const tmp = Object.assign({}, additionalParams);
    let params = new HttpParams();
    for (const key in tmp) {
      if (tmp.hasOwnProperty(key)) {
        params = params.append(key, tmp[key]);
      }
    }
    this.store.dispatch(new PostersLoad({params, filters: tmp}));
  }

}
