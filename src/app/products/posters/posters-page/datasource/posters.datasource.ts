import { AppDataSource } from '../../../../ui/table/datasource';
import { createSelector } from '@ngrx/store';
import { ProductsState } from '../../../store/reducers';
import { selectProductsState } from '../../../store/reducers/products.reducer';
import { PostersModel } from '../../../../core/models/posters.model';
import { Observable } from 'rxjs';

export const selectPostersState = createSelector(
  selectProductsState,
  (state: ProductsState) => state.posters
);

export class PostersDatasource extends AppDataSource<PostersModel> {

  public connect(): Observable<PostersModel[]> {
    this.initSelectors<PostersModel>(selectPostersState);
    return this.initSubscriptions();
  }

}
