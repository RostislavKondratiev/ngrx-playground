import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsListComponent } from './shared/components/products-list/products-list.component';
import { ProductsComponent } from './products.component';
import { BookDetailsPageComponent } from './books/book-details-page/book-details-page.component';
import { BooksPageComponent } from './books/books-page/books-page.component';
import { MagazineDetailsPageComponent } from './magazines/magazine-details-page/magazine-details-page.component';
import { MagazinesPageComponent } from './magazines/magazines-page/magazines-page.component';
import { ProductsRoutingModule } from './products-routing.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { effects } from './store/effects';
import { ProductsItemComponent } from './shared/components/products-item/products-item.component';
import { ProductDetailsComponent } from './shared/components/product-details/product-details.component';
import { NewsPapersPageComponent } from './news-papers/news-papers-page/news-papers-page.component';
import { PostersPageComponent } from './posters/posters-page/posters-page.component';
import { SortModule } from '../ui/sort/sort.module';
import { TableModule } from '../ui/table/data-table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NotificationModule } from '../ui/notification/notification.module';

const COMPONENTS = [
  ProductsListComponent,
  ProductsItemComponent,
  ProductDetailsComponent,
  ProductsComponent,
  BookDetailsPageComponent,
  BooksPageComponent,
  MagazineDetailsPageComponent,
  MagazinesPageComponent,
  NewsPapersPageComponent,
  PostersPageComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature(effects),
    NotificationModule,
    ReactiveFormsModule,
    TableModule,
    SortModule
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: []
})
export class ProductsModule {

}
