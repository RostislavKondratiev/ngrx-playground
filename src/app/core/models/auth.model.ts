export interface SignUpModel {
  email: string;
  username: string;
  password: string;
  password_confirm: string;
}

export interface LoginModel {
  email: string;
  password: string;
}

export interface UserModel {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  username: string;
  photo: {photo: string};
  token: string;
}
