import { TableMeta } from '../../ui/table/ds-meta';

export interface PostersModel {
  id: number;
  title: string;
  price: number;
}

export interface PostersResponse {
  data: PostersModel[];
  meta: TableMeta;
}
