import { Action } from '@ngrx/store';
import { LoginModel, SignUpModel } from '../../models/auth.model';

export const LOGIN = '[Auth] LoginModel';
export const LOGIN_SUCCESS = '[Auth] LoginModel Success';
export const LOGIN_FAIL = '[Auth] LoginModel Fail';

export const SIGNUP = '[Auth] Sign Up';
export const SIGNUP_SUCCESS = '[Auth] Sign Up Success';
export const SIGNUP_FAIL = '[Auth] Sign Up Fail';

export class Login implements Action {
  readonly type = LOGIN;

  constructor(public payload: LoginModel) {
  }
}

export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;
}

export class LoginFail implements Action {
  readonly type = LOGIN_FAIL;

  constructor(public payload: any) {
  }
}

export class SignUp implements Action {
  readonly type = SIGNUP;

  constructor(public payload: SignUpModel) {
  }
}

export class SignUpSuccess implements Action {
  readonly type = SIGNUP_SUCCESS;
}

export class SignUpFail implements Action {
  readonly type = SIGNUP_FAIL;

  constructor(public payload: any) {
  }
}

export type AuthAction =
  | Login
  | LoginSuccess
  | LoginFail
  | SignUp
  | SignUpSuccess
  | SignUpFail;
