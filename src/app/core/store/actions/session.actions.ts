import { Action } from '@ngrx/store';
import { UserModel } from '../../models/auth.model';

export const GET_SESSION = '[Session] Get Session';
export const GET_SESSION_SUCCESS = '[Session] Get Session Success';
export const GET_SESSION_FAIL = '[Session] Get Session Fail';
export const SET_SESSION = '[Session] Set Session';
export const CLEAN_SESSION = '[Session] Clean Session';

export class GetSession implements Action {
  readonly type = GET_SESSION;
}

export class GetSessionSuccess implements Action {
  readonly type = GET_SESSION_SUCCESS;

  constructor(public payload: UserModel) {
  }
}

export class GetSessionFail implements Action {
  readonly type = GET_SESSION_FAIL;
}

export class SetSession implements Action {
  readonly type = SET_SESSION;

  constructor(public payload: UserModel) {
  }
}

export class CleanSession implements Action {
  readonly type = CLEAN_SESSION;
}

export type SessionActions =
  | GetSession
  | GetSessionSuccess
  | GetSessionFail
  | SetSession
  | CleanSession;
