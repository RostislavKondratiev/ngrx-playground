import { RouterEffects } from './router.effects';
import { AuthEffects } from './auth.effects';
import { SessionEffects } from './session.effects';

export const effects: any[] = [RouterEffects, AuthEffects, SessionEffects];

export * from './router.effects';
