import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as rootActions from '../actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions) {
  }

  // @Effect()
  // public signUp$ = this.actions$
  //   .pipe(
  //     ofType(rootActions.SIGNUP),
  //     map((action: rootActions.SignUp) => action.payload),
  //     switchMap((data) => {
  //       return this.authService
  //         .signUp(data)
  //         .pipe(
  //           switchMap(() => [
  //             new rootActions.SignUpSuccess(),
  //             new rootActions.Go({path: ['/authorization', 'login']})
  //           ]),
  //           catchError((error) => of(new rootActions.SignUpFail(error)))
  //         );
  //     })
  //   );
  //
  // @Effect()
  // public login$ = this.actions$
  //   .pipe(
  //     ofType(rootActions.LOGIN),
  //     map((action: rootActions.Login) => action.payload),
  //     switchMap((data) => {
  //       return this.authService
  //         .login(data)
  //         .pipe(
  //           switchMap((user) => [
  //             new rootActions.LoginSuccess(),
  //             new rootActions.SetSession(user),
  //             new rootActions.Go({path: ['/products']})
  //           ]),
  //           catchError((error) => of(new rootActions.LoginFail(error)))
  //         );
  //     })
  //   );
}
