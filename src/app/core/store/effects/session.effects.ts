import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import * as sessionActions from '../actions/session.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserModel } from '../../models/auth.model';
import { LocalStorage } from '../../../helpers/localStorage';

@Injectable()
export class SessionEffects {

  constructor(private actions$: Actions) {
  }

  @Effect()
  public setSession$ = this.actions$
    .pipe(
      ofType(sessionActions.SET_SESSION),
      map((action: sessionActions.SetSession) => action.payload),
      map((user) => {
        LocalStorage.setStorageItem('user', user);
        return new sessionActions.GetSession();
      }));

  @Effect()
  public getSession$ = this.actions$
    .pipe(
      ofType(sessionActions.GET_SESSION),
      switchMap(() => {
        return LocalStorage
          .getStorageItem('user')
          .pipe(
            map((user: UserModel) => new sessionActions.GetSessionSuccess(user)),
            catchError(() => of(new sessionActions.GetSessionFail()))
          );
      })
    );

  @Effect()
  public cleanSession$ = this.actions$
    .pipe(
      ofType(sessionActions.CLEAN_SESSION),
      map(() => {
        LocalStorage.setStorageItem('user', null);
        return new sessionActions.GetSession();
      })
    );
}
