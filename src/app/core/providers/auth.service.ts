import { Injectable } from '@angular/core';
import { AuthHttpService } from '../http-services/auth-http.service';

@Injectable()
export class AuthService {
  constructor(private authHttp: AuthHttpService) {}
}
