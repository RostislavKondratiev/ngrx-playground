import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromCore from './store/reducers';
import { HttpClientModule } from '@angular/common/http';
import { AuthHttpService } from './http-services/auth-http.service';
import { AuthService } from './providers/auth.service';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ProductsHttpService } from './http-services/products-http.service';
import { EffectsModule } from '@ngrx/effects';
import { PostersHttpService } from './http-services/posters-http.service';

const HTTP_SERVICES = [
  AuthHttpService,
  ProductsHttpService,
  PostersHttpService
];

const PROVIDERS = [
  AuthService
];


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(fromCore.reducers),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'routerReducer'
    }),
    EffectsModule.forRoot([])
  ],
  providers: [
    ...HTTP_SERVICES,
    ...PROVIDERS
  ],
  exports: [
    StoreModule,
    StoreRouterConnectingModule,
  ]
})
export class CoreModule { }
