import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../../helpers/api-config';
import { PostersResponse } from '../models/posters.model';

@Injectable()
export class PostersHttpService {
  constructor(private http: HttpClient) {
  }

  getPosters(params) {
    return this.http.get<PostersResponse>(ApiConfig.posters, {params});
  }
}
