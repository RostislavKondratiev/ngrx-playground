import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../../helpers/api-config';

@Injectable()
export class ProductsHttpService {
  constructor(private http: HttpClient) {
  }

  public getBooks() {
    return this.http.get(ApiConfig.books);
  }

  public getBookById(id: number) {
    return this.http.get(`${ApiConfig.books}/${id}`);
  }

  public getMagazines() {
    return this.http.get(ApiConfig.magazines);
  }

  public getMagazineById(id: number) {
    return this.http.get(`${ApiConfig.magazines}/${id}`);
  }

  public getNewsPapers() {
    return this.http.get(ApiConfig.newspapers);
  }
}
