import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'users'},
  {path: 'users', loadChildren: './users/users.module#UsersModule'},
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
