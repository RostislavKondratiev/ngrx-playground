import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UserDetailsPageComponent } from './containers/user-details-page/user-details-page.component';
import { UserDetailsFormComponent } from './components/user-details-form/user-details-form.component';
import { UsersListPageComponent } from './containers/users-list-page/users-list-page.component';
import { AppDataTable } from '../../ui/table/table';
import { TableModule } from '../../ui/table/data-table.module';

const COMPONENTS = [
  UserDetailsPageComponent,
  UserDetailsFormComponent,
  UsersListPageComponent,
];

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    TableModule,
  ],
  declarations: [
    ...COMPONENTS
  ],
  providers: [],
})
export class UsersModule {

}
