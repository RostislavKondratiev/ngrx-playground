import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersListPageComponent } from './containers/users-list-page/users-list-page.component';
import { UserDetailsPageComponent } from './containers/user-details-page/user-details-page.component';

const ROUTES: Routes = [
  {path: '', component: UsersListPageComponent},
  {path: ':id', component: UserDetailsPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class UsersRoutingModule {

}
