import { ComponentRef } from '@angular/core';
import { OverlayRef } from '@angular/cdk/overlay';
import { NotificationConfig } from './notification.config';
import { Subject } from 'rxjs';

export class NotificationRef {

  public closeState = new Subject<any>();

  constructor(
    public containerRef: ComponentRef<any>,
    public overlayRef: OverlayRef,
    public config: NotificationConfig
  ) {
    setTimeout(() => {
      this.overlayRef.dispose();
      this.closeState.next(null);
      this.closeState.complete();
    }, config.duration || 3000);
  }

  public close(value = null) {
    this.closeState.next(value);
    this.closeState.complete();
    this.overlayRef.dispose();
  }
}
