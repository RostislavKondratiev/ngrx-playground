import { Component, Input, TemplateRef } from '@angular/core';
import { NotificationConfig } from '../notification.config';
import { NotificationRef } from '../notification-ref';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {
  @Input() public config: NotificationConfig;
  @Input() public notificationRef: NotificationRef;

  public title: string;
  public message: string;
  public customTemplate: TemplateRef<any>;

  public set properties(config: NotificationConfig) {
    this.title = config.title;
    this.message = config.message;
    this.customTemplate = config.template;
  }
}
