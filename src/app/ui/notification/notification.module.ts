import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationService } from './notification.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { NotificationComponent } from './notification/notification.component';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule
  ],
  declarations: [
    NotificationComponent
  ],
  entryComponents: [
    NotificationComponent,
  ],
  providers: [
    NotificationService
  ]
})
export class NotificationModule {

}
