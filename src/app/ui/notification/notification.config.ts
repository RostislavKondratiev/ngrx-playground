import { TemplateRef } from '@angular/core';

export interface NotificationConfig {
  title: string;
  message: string;
  duration?: number;
  template?: TemplateRef<any>;
}
