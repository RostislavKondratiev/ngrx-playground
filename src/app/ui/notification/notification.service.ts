import { ComponentRef, Injectable } from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { NotificationConfig } from './notification.config';
import { ComponentPortal } from '@angular/cdk/portal';
import { NotificationComponent } from './notification/notification.component';
import { NotificationRef } from './notification-ref';

@Injectable()
export class NotificationService {
  constructor(private overlay: Overlay) {
  }

  public openNotification(config: NotificationConfig) {
    const overlayRef = this.overlay.create({
      width: '100%'
    });
    const notificationPortal = new ComponentPortal(NotificationComponent);
    const containerRef: ComponentRef<NotificationComponent> = overlayRef.attach(notificationPortal);
    const notificationRef = new NotificationRef(containerRef, overlayRef, config);
    containerRef.instance.config = config;
    containerRef.instance.properties = config;
    containerRef.instance.notificationRef = notificationRef;
    return notificationRef;
  }

}
