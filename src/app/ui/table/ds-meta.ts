import { HttpParams } from '@angular/common/http';

export interface TableMeta {
  [prop: string]: any;

  total: number;
  page: number;
  per_page: number;
}

export interface TableResponse<T> {
  data: T[];
  meta: TableMeta;
}

export interface TableDataBase<T> {
  load(params): any;
}

export interface TableTarget {
  success: any;
  fail: any;
}

export interface TableLoadBody {
  params: HttpParams;
  filters: any;
}

export interface TableSelectors {
  entities: any;
  loading: any;
  meta: any;
}
