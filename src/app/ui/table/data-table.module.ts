import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { AppDataTable } from './table';
import { AppCell, AppCellDef, AppColumnDef, AppHeaderCell, AppHeaderCellDef } from './cell';
import { AppHeaderRow, AppHeaderRowDef, AppRow, AppRowDef } from './row';
import { FilterContainerComponent } from './filters/filter-container/filter-container.component';
import { FilterDirective } from './filters/filter.directive';
import { TableWrapperComponent } from './table-wrapper/table-wrapper.component';
import { PaginationModule } from '../pagination/pagination.module';

const COMPONENTS = [
  FilterContainerComponent,
  FilterDirective,
  TableWrapperComponent,
  AppDataTable,
  AppCellDef,
  AppHeaderCellDef,
  AppColumnDef,
  AppHeaderCell,
  AppCell,
  AppHeaderRow,
  AppRow,
  AppHeaderRowDef,
  AppRowDef
];

@NgModule({

  imports: [
    CdkTableModule,
    CommonModule,
    PaginationModule,
  ],
  exports: [
    COMPONENTS
  ],
  declarations: [
    COMPONENTS
  ],
})
export class TableModule {
}
