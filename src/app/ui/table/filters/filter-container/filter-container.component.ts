import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { FilterDirective } from '../filter.directive';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { deepCompare } from '../../../../helpers/utils';

@Component({
  selector: 'app-filter-container',
  templateUrl: './filter-container.component.html',
  styleUrls: ['./filter-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterContainerComponent implements AfterContentInit {

  public fg = new FormGroup({});
  public filterChanged = false;
  @ContentChildren(FilterDirective) filters: QueryList<FilterDirective>;
  @Output() filterChange = new EventEmitter();
  @ViewChild('container', {read: ViewContainerRef}) private container: ViewContainerRef;
  private initial: any;
  private contentChanged$: Subscription;

  // @HostBinding('class.space-between')
  @Input() public spaceBetween = false;

  constructor(private cd: ChangeDetectorRef) {
  }

  public ngAfterContentInit() {
    this.init();

    this.contentChanged$ = this.filters.changes.subscribe(() => {
      this.init();
    });
    this.initial = this.fg.value;
  }

  public clear($event) {
    $event.preventDefault();
    this.fg.patchValue(this.initial);
  }

  private init() {
    this.container.clear();
    this.filters.forEach((v) => {
      if (!this.fg.get(v.name)) {
        this.fg.setControl(v.name, new FormControl());
      }
      this.container.createEmbeddedView(v.template, {
        $implicit: {
          control: this.fg.get(v.name)
        }
      });
      this.cd.detectChanges();
    });
    this.fg.valueChanges.pipe(
      debounceTime(300)
    ).subscribe((v) => {
      this.filterChanged = deepCompare(this.initial, v) === false;
      this.filterChange.emit(v);
      this.cd.detectChanges();
    });
    this.cd.markForCheck();
  }

}
