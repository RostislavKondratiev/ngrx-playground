import { Directive, Input, Optional, TemplateRef } from '@angular/core';
import { FilterContainerComponent } from './filter-container/filter-container.component';

@Directive({
  selector: '[appFilter]'
})
export class FilterDirective {

  @Input('appFilter')
  public name = '';

  constructor(public template: TemplateRef<any>) {
  }

}
