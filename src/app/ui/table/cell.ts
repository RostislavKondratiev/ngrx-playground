// tslint:disable use-host-property-decorator use-input-property-decorator directive-class-suffix component-class-suffix
// tslint:disable max-classes-per-file no-input-rename directive-selector
import { Directive, ElementRef, Input } from '@angular/core';
import { CdkCell, CdkCellDef, CdkColumnDef, CdkHeaderCell, CdkHeaderCellDef, } from '@angular/cdk/table';

export const _AppCellDef = CdkCellDef;
export const _AppHeaderCellDef = CdkHeaderCellDef;
export const _AppColumnDef = CdkColumnDef;
export const _AppHeaderCell = CdkHeaderCell;
export const _AppCell = CdkCell;

@Directive({
  selector: '[appCellDef]',
  providers: [{provide: CdkCellDef, useExisting: AppCellDef}]
})
export class AppCellDef extends _AppCellDef {
}

@Directive({
  selector: '[appHeaderCellDef]',
  providers: [{provide: CdkHeaderCellDef, useExisting: AppHeaderCellDef}]
})
export class AppHeaderCellDef extends _AppHeaderCellDef {
}

@Directive({
  selector: '[appColumnDef]',
  providers: [{provide: CdkColumnDef, useExisting: AppColumnDef}],
})
export class AppColumnDef extends _AppColumnDef {
  @Input('appColumnDef') public name: string;
}

@Directive({
  selector: 'app-header-cell',
  host: {
    class: 'app-header-cell',
    role: 'columnheader',
  },
})
export class AppHeaderCell extends _AppHeaderCell {
  constructor(columnDef: CdkColumnDef,
              elementRef: ElementRef) {
    super(columnDef, elementRef);
    elementRef.nativeElement.classList.add(`app-column-${columnDef.cssClassFriendlyName}`);
  }
}

@Directive({
  selector: 'app-cell',
  host: {
    class: 'app-cell',
    role: 'gridcell',
  },
})
export class AppCell extends _AppCell {
  constructor(columnDef: CdkColumnDef,
              elementRef: ElementRef) {
    super(columnDef, elementRef);
    elementRef.nativeElement.classList.add(`app-column-${columnDef.cssClassFriendlyName}`);
  }
}
