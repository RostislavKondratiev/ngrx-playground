// tslint:disable use-host-property-decorator use-input-property-decorator directive-class-suffix component-class-suffix
// tslint:disable max-classes-per-file

import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { CDK_TABLE_TEMPLATE, CdkTable } from '@angular/cdk/table';

export const _AppDataTable = CdkTable;

@Component({
  selector: 'app-data-table',
  exportAs: 'appDataTable',
  template: CDK_TABLE_TEMPLATE,
  styleUrls: ['table.scss'],
  host: {
    class: 'app-data-table'
  },
  encapsulation: ViewEncapsulation.None,
  // preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppDataTable<T> extends _AppDataTable<T> {
}
