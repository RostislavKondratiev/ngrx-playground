// tslint:disable use-host-property-decorator use-input-property-decorator directive-class-suffix component-class-suffix
// tslint:disable max-classes-per-file no-input-rename
import { ChangeDetectionStrategy, Component, Directive, ViewEncapsulation } from '@angular/core';
import { CDK_ROW_TEMPLATE, CdkHeaderRow, CdkHeaderRowDef, CdkRow, CdkRowDef, } from '@angular/cdk/table';

/** Workaround for https://github.com/angular/angular/issues/17849 */
export const _AppHeaderRowDef = CdkHeaderRowDef;
export const _AppCdkRowDef = CdkRowDef;
export const _AppHeaderRow = CdkHeaderRow;
export const _AppRow = CdkRow;

@Directive({
  selector: '[appHeaderRowDef]',
  providers: [{provide: CdkHeaderRowDef, useExisting: AppHeaderRowDef}],
  inputs: ['columns: appHeaderRowDef'],
})
export class AppHeaderRowDef extends _AppHeaderRowDef {
}

@Directive({
  selector: '[appRowDef]',
  providers: [{provide: CdkRowDef, useExisting: AppRowDef}],
  inputs: ['columns: appRowDefColumns', 'when: appRowDefWhen'],
})
export class AppRowDef<T> extends _AppCdkRowDef<T> {
}

@Component({
  selector: 'app-header-row',
  template: CDK_ROW_TEMPLATE,
  host: {
    class: 'app-header-row',
    role: 'row',
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  exportAs: 'appHeaderRow',
  // preserveWhitespaces: false,
})
export class AppHeaderRow extends _AppHeaderRow {
}

@Component({
  selector: 'app-row',
  template: CDK_ROW_TEMPLATE,
  host: {
    class: 'app-row',
    role: 'row',
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  exportAs: 'appRow',
  // preserveWhitespaces: false,
})
export class AppRow extends _AppRow {
}
