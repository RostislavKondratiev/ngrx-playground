import { Action } from '@ngrx/store';
import { TableLoadBody, TableResponse } from '../ds-meta';

export abstract class TableLoad implements Action {
  public type: string;

  constructor(public payload: TableLoadBody) {
  }
}

export abstract class TableLoadSuccess<T> implements Action {
  public type: string;

  constructor(public payload: TableResponse<T>) {
  }
}

export abstract class TableLoadFail implements Action {
  public type: string;

  constructor(public payload: any) {
  }
}
