import { EntityState } from '@ngrx/entity';
import { TableMeta } from '../ds-meta';
import { TableLoad, TableLoadFail, TableLoadSuccess } from './table.actions';

export interface TableState<T> extends EntityState<T> {
  loading: boolean;
  error: null;
  meta: TableMeta;
}

export abstract class TableReducer<T> {

  protected adapter;

  constructor(adapter) {
    this.adapter = adapter;
  }

  load(state: TableState<T>, action: TableLoad) {
    return {...state, loading: true, meta: {...action.payload.filters}};
  }

  loadSuccess(state: TableState<T>, action: TableLoadSuccess<T>) {
    return this.adapter.addAll(action.payload.data,
      {...state, meta: {...state.meta, ...action.payload.meta}, loading: false}
    );
  }

  loadFail(state: TableState<T>, action: TableLoadFail) {
    return {
      ...state,
      loading: false,
      error: action.payload
    };
  }
}
