import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TableLoad } from './table.actions';
import { HttpParams } from '@angular/common/http';
import { TableTarget } from '../ds-meta';

@Injectable()
export abstract class TableEffects {

  constructor(protected actions$: Actions) {
  }

  protected fetch(type: string, request: (params: HttpParams) => Observable<any>, target: TableTarget) {
    return this.actions$
      .pipe(
        ofType(type),
        switchMap((action: TableLoad) => request(action.payload.params)
          .pipe(
            map((response) => new target.success(response)),
            catchError((err) => new target.fail(err))
          )
        )
      );
  }
}
