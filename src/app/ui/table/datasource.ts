import { BehaviorSubject, merge, Observable, Subject, Subscription } from 'rxjs';
import { TableDataBase, TableMeta, TableSelectors } from './ds-meta';
import { SortDirective } from '../sort/sort.directive';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { map, skip, tap, throttleTime } from 'rxjs/operators';
import { createSelector, Store } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';
import { FilterContainerComponent } from './filters/filter-container/filter-container.component';

export abstract class AppDataSource<T> implements DataSource<T> {

  public meta$: Observable<TableMeta>;
  public loading$: Observable<Boolean>;
  public pageState = new BehaviorSubject<number>(1);
  public loadState = new Subject<any>();
  private _filtersState = new BehaviorSubject<any>({});
  private _meta: TableMeta;
  private sort$: Subscription;
  private _filtersState$: Subscription;
  private pageState$: Subscription;
  private filters: FilterContainerComponent;

  protected selectors: TableSelectors = {
    entities: null,
    meta: null,
    loading: null
  };

  public get metaSync() {
    if (this._meta) {
      return this._meta;
    }
    return null;
  }

  public get page(): number {
    if (this.metaSync) {
      return this.metaSync.page;
    }
    return 1;
  }

  public set page(page: number) {
    this.pageState.next(page);
  }

  constructor(private db: TableDataBase<T>, private sort: SortDirective, protected store: Store<any>) {
    // if (sort) {
    //   this.sort$ = merge(sort.sortChange).subscribe((v) => {
    //     // this.refresh();
    //     console.log(123);
    //   });
    // }
    // this.load({page: 1});
    // this._filtersState$ = this._filtersState.asObservable().pipe(skip(1)).subscribe((v) => {
    //   this.load({page: 1});
    // });
    // this.pageState$ = this.pageState.asObservable().pipe(skip(1)).subscribe((v) => {
    //   this.load({page: v});
    // });
  }

  public registerFilters(filter: FilterContainerComponent) {
    this.filters = filter;
    this.filters.fg.patchValue(this.metaSync);
  }

  public hasNext() {
    if (!this.metaSync) {
      return false;
    }
    return (this.metaSync.page * this.metaSync.per_page) < this.metaSync.total;
  }

  public setFilters(filters) {
    if (filters.date) {
      filters.from = filters.date.from;
      filters.to = filters.date.to;
    }
    this._filtersState.next(filters);
  }

  public abstract connect(): Observable<T[]>;

  public disconnect(collectionViewer: CollectionViewer): void {
    if (this.sort$) {
      this.sort$.unsubscribe();
    }
    this._filtersState$.unsubscribe();
    this.pageState$.unsubscribe();
    this.loadState.complete();
  }

  public load(params) {
    this.makeRequest(params);
  }

  // public refresh() {
  //   range(1, this.page).pipe(
  //     map((v) => this.makeRequest({page: v})),
  //     combineAll()
  //   )
  //     .subscribe((res: any[]) => {
  //       const data = res.map((v) => v.data).reduce((a, b) => a.concat(b), []);
  //       this._state.next(data);
  //       this.meta.next(res[res.length - 1].meta);
  //     });
  // }

  public loadNext() {
    this.page = this.page + 1;
  }

  private makeRequest(params: any = {}) {
    const filtersValue = Object.assign({}, this._filtersState.value);
    let sortParams = {};
    if (this.sort && this.sort.active && this.sort.direction) {
      sortParams = {
        _sort: this.sort.active,
        _order: this.sort.direction
      };
    }
    for (const value in filtersValue) {
      if (filtersValue.hasOwnProperty(value)) {
        if (!filtersValue[value]) {
          delete filtersValue[value];
        }
      }
    }
    params = Object.assign({}, sortParams, filtersValue);
    this.db.load(params);
  }

  protected initSubscriptions(): Observable<T[]> {
    this.loading$ = this.store.select(this.selectors.loading);

    this.loadState.asObservable().pipe(
      throttleTime(300)
    ).subscribe((v) => {
      this.load(v);
    });

    this.meta$ = this.store.select(this.selectors.meta).pipe(
      tap((v) => {
        if (!this._meta && v._sort && v._order) {
          this.sort.setSortValue(v._sort, v._order);
          this.loadState.next({page: v.page || 1});
        }
        this._meta = v;
      }),
    )
    ;

    this._filtersState$ = this._filtersState.asObservable()
      .pipe(skip(1)).subscribe((v) => {
        this.loadState.next({page: 1});
      });

    if (this.sort) {
      this.sort$ = merge(this.sort.sortChange).subscribe((v) => {
        this.loadState.next({page: this.metaSync.page || 1});
      });
    }

    return this.store.select(this.selectors.entities)
      .pipe(
        map((data) => Object.values(data))
      );
  }

  protected initSelectors<K>(selectState: any) {

    const adapter = createEntityAdapter<K>();
    const selectors = adapter.getSelectors();

    this.selectors.entities = createSelector(
      selectState,
      selectors.selectEntities
    );

    this.selectors.loading = createSelector(
      selectState,
      (state: any) => state.loading
    );

    this.selectors.meta = createSelector(
      selectState,
      (state: any) => state.meta
    );
  }

}
