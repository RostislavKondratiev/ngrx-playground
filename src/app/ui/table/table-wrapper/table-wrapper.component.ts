import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  OnDestroy,
  TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import { AppDataTable } from '../table';
import { AppDataSource } from '../datasource';
import { TableMeta } from '../ds-meta';
import { FilterContainerComponent } from '../filters/filter-container/filter-container.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-table-wrapper',
  templateUrl: './table-wrapper.component.html',
  styleUrls: ['./table-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TableWrapperComponent implements AfterViewInit, OnDestroy {
  @ContentChild(AppDataTable) public table: AppDataTable<any>;
  @ContentChild(FilterContainerComponent) public filters: FilterContainerComponent;
  @ContentChild('metaTemplate') public metaTemplate: TemplateRef<TableMeta>;
  public ds: AppDataSource<any>;
  private until: Subject<void> = new Subject<void>();

  public get meta() {
    if (!this.ds || !this.ds.metaSync) {
      return null;
    }
  }

  constructor(private cd: ChangeDetectorRef) {
  }

  public ngAfterViewInit(): void {
    this.ds = this.table.dataSource as AppDataSource<any>;
    this.cd.detectChanges();
    this.ds.meta$.pipe(
      takeUntil(this.until)
    ).subscribe(() => {
      this.cd.detectChanges();
    });
    if (this.filters) {
      this.ds.registerFilters(this.filters);
      this.filters.filterChange
        .pipe(
          takeUntil(this.until)
        )
        .subscribe((v) => {
          this.ds.setFilters(v);
        });
    }
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.complete();
  }

}
