import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppErrorDirective } from './app-error.directive';
import { AppServerErrorDirective } from './app-server-error.directive';

const DIRECTIVES = [
  AppErrorDirective,
  AppServerErrorDirective,
];

@NgModule({
  imports: [CommonModule],
  declarations: [
    ...DIRECTIVES
  ],
  exports: [
    ...DIRECTIVES
  ]
})
export class AppErrorsModule {

}
