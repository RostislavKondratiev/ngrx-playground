import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  HostBinding,
  Injector,
  Input,
  ViewContainerRef
} from '@angular/core';
import { InputContainerComponent } from '../input-container/input-container.component';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { filter } from 'rxjs/operators';

//tslint:disable


@Directive({
  selector: 'app-error',
})
export class AppErrorDirective implements AfterViewInit {
  @Input() public key: string;
  @Input() public novalidate = false;
  @HostBinding('hidden') public hidden: boolean = true;

  private control: FormControl;
  private form: FormGroupDirective | NgForm;
  public container: InputContainerComponent;

  constructor(private injector: Injector,
              private cd: ChangeDetectorRef) {
  }

  public ngAfterViewInit() {
    this.container = this.injector.get(InputContainerComponent);
    this.control = this.container.control;
    this.form = this.container.input.parent;
    this.container.statusChanges$
      .pipe(
        filter(() => !this.novalidate)
      )
      .subscribe((invalid) => {
          this.hidden = !(invalid && this.control.hasError(this.key));
          this.cd.markForCheck();
      });
  }
}

