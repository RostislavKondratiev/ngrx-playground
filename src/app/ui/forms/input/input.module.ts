import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppInputDirective } from './input.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AppInputDirective,
  ],
  exports: [
    AppInputDirective
  ]
})
export class AppInputModule {

}
