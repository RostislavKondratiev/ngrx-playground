import {
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  DoCheck,
  HostBinding,
  Inject,
  Input,
  Optional,
  QueryList,
  ViewEncapsulation
} from '@angular/core';
import {OPTION_PARENT_COMPONENT, OptionComponent} from '../option/option.component';

@Component({
  selector: 'app-options-group',
  templateUrl: './options-group.component.html',
  styleUrls: ['./options-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class OptionsGroupComponent implements DoCheck {
  @Input() public label = '';
  @Input() public nonGroup = false;
  @ContentChildren(OptionComponent, {descendants: true}) public options: QueryList<OptionComponent>;
  @ContentChildren(OptionsGroupComponent) public groups: QueryList<OptionsGroupComponent>;

  @HostBinding('hidden')
  public hidden = false;

  constructor(@Optional() @Inject(OPTION_PARENT_COMPONENT) private _parent) {
  }

  public ngDoCheck() {
    this.hidden = this.hideGroup(this.groups) || this.hideGroup(this.options);
  }

  private hideGroup(type) {
    return type && type.length > 0 ? !type.find((item) => !item.hidden) : false;
  }

}
