import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  forwardRef,
  HostBinding,
  Injector,
  Input,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { InputPositions } from '../../input-positions';
import { FormFieldControl } from '../../form-field-control';

const APP_RADIO_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RadioGroupComponent),
  multi: true
};

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [APP_RADIO_VALUE_ACCESSOR]
})
export class RadioGroupComponent implements ControlValueAccessor, AfterContentInit {
  public radioChanges$: Subject<any> = new Subject<any>();
  private _value = null;
  private _disabled: boolean;
  private directive: FormFieldControl;

  public onChange: any;
  public onTouched: any;

  @HostBinding('style.padding-left.px')
  public get inputPaddingLeft() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  @HostBinding('style.padding-right.px')
  public get inputPaddingRight() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.RightTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  public get value() {
    return this._value;
  }

  public set value(val) {
    if (!this.disabled) {
      this._value = val;
      this.radioChanges$.next(this._value);
      this.onChange(this.value);
    }
  }

  @HostBinding('class.radio-vertical')
  @Input() public vertical = false;

  @HostBinding('class.disabled')
  public get disabled() {
    return this._disabled;
  }

  public set disabled(isDisabled: boolean) {
    this.setDisabledState(isDisabled);
  }

  constructor(private injector: Injector) {
  }

  public ngAfterContentInit() {
    this.directive = this.injector.get(FormFieldControl, null);
  }

  public registerOnChange(fn: (value: any) => any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  public writeValue(value) {
    this._value = value;
  }

  public setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }
}
