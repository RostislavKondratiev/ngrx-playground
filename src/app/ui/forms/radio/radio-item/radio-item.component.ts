import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';
import { RadioGroupComponent } from '../radio-group/radio-group.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-radio-item',
  templateUrl: './radio-item.component.html',
  styleUrls: ['./radio-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class RadioItemComponent implements AfterContentInit, OnDestroy {
  @Input() public value: any;
  public until: Subject<void> = new Subject<void>();
  public checked = false;

  @HostBinding('class.disabled')
  @Input() public disabled = false;

  constructor(private radioGroup: RadioGroupComponent,
              private cd: ChangeDetectorRef) {
  }

  public ngAfterContentInit() {
    this.radioGroup
      .radioChanges$
      .pipe(
        takeUntil(this.until)
      )
      .subscribe((val) => {
        this.checked = val === this.value;
        this.cd.markForCheck();
      });
  }

  @HostListener('click')
  public onClick() {
    if (!this.disabled) {
      this.radioGroup.value = this.value;
    }
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.complete();
  }
}
