import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  DoCheck,
  Inject,
  Input,
  OnDestroy,
  QueryList,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControl } from '../form-field-control';
import { Observable ,  Subject ,  merge } from 'rxjs';
import { map, share, takeUntil, tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { StrategyHandler, validationStrategy } from '../errors/errors.strategies';

import { AffixPositions } from './affix-positions';
import { AppErrorDirective } from '../errors/app-error.directive';
import { AppAffixDirective } from './affix.directive';
import { AppHintDirective } from './hint.directive';
import { AppPlaceholderDirective } from './placeholder.directive';
import { AppInputDirective } from '../input/input.directive';
import { InputPositions } from '../input-positions';
import { AppLabelDirective } from './label.directive';

//tslint:disable

@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class InputContainerComponent implements AfterContentInit, DoCheck, OnDestroy {
  @Input() public novalidate = false;
  @Input() public labelColumn = false;
  @ContentChild(FormFieldControl) public input: FormFieldControl;
  @ContentChild(AppHintDirective) public hint: AppHintDirective;
  @ContentChild(AppPlaceholderDirective) public placeholder: AppPlaceholderDirective;
  @ContentChild(AppLabelDirective) public label: AppLabelDirective;
  @ContentChildren(AppAffixDirective) public affixes: QueryList<AppAffixDirective>;
  @ContentChildren(AppErrorDirective) public errors: QueryList<AppErrorDirective>;
  public inputIndent: number;
  public statusChanges$: Observable<string | boolean>;
  public availableCustom: string[] = [];
  public enableDefaultPlaceholderCheck = true;
  public inputPosition: Array<'left' | 'right' | 'left-transparent' | 'right-transparent'> = [];
  public control: FormControl;

  private until: Subject<void> = new Subject<void>();

  @Input()
  public set validationStrategy(strategy: StrategyHandler) {
    this.errorStrategy = strategy;
  }

  constructor(@Inject(validationStrategy) private errorStrategy: StrategyHandler,
              private cd: ChangeDetectorRef) {
  }

  public ngAfterContentInit() {
    this.availableCustom = this.errors.map((v) => v.key);
    this.errors.changes.pipe(takeUntil(this.until)).subscribe((list: QueryList<AppErrorDirective>) => {
      this.availableCustom = list.map((v) => v.key);
    });
    if(!this.input.disabled && this.input.required) {
      this.label.required = true;
    }
    this.control = this.input.control.control as FormControl;
    this.statusChanges$ = merge(this.control.statusChanges, this.input.stateChange$)
      .pipe(
        takeUntil(this.until),
        map(() => this.input.hasError = this.errorStrategy(this.control, this.input.parent)),
        tap((state) => {
          if (this.hint) {
            this.hint.hidden = state;
          }
        }),
        share(),
      );
    this.affixPosition();
  }

  public ngDoCheck() {
    if (this.enableDefaultPlaceholderCheck && this.placeholder && this.input) {
      this.placeholder.hidden = !!this.input.control.value;
    }
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.complete();
  }

  private affixPosition() {
    if (this.affixes && this.affixes.length > 0) {
      this.affixes.forEach((affix: AppAffixDirective) => {
        switch (affix.type) {
          case AffixPositions.Suffix:
            this.input.inputPosition.push(affix.transparent ? InputPositions.RightTransparent : InputPositions.Right);
            break;
          case AffixPositions.Prefix:
            this.input.inputPosition.push(affix.transparent ? InputPositions.LeftTransparent : InputPositions.Left);
            if (this.placeholder) {
              this.placeholder.left = affix.elem.nativeElement.offsetWidth + (affix.transparent ? 3 : 15);
            }
            break;
        }
        if (affix.transparent) {
          this.input.inputIndent = affix.elem.nativeElement.offsetWidth;
          this.cd.detectChanges();
        }
      });
    }
  }
}
