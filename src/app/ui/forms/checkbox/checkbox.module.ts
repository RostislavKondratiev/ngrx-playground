import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxComponent } from './checkbox.component';
import { AppCheckboxDirective } from './checkbox.directive';
import { FormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';

@NgModule({
  imports: [
    CommonModule,
    InlineSVGModule,
    FormsModule
  ],
  declarations: [
    CheckboxComponent,
    AppCheckboxDirective,
  ],
  exports: [
    CheckboxComponent,
    AppCheckboxDirective
  ]
})
export class AppCheckboxModule {

}
