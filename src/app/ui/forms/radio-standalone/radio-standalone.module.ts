import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioStandaloneDirective } from './radio-standalone.directive';
import { RadioGroupStandaloneDirective } from './radio-group-standalone.directive';

const DIRECTIVES = [
  RadioGroupStandaloneDirective,
  RadioStandaloneDirective
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...DIRECTIVES
  ],
  exports: [
    ...DIRECTIVES
  ]
})
export class AppRadioStandaloneModule {

}
