import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Host,
  HostListener, Injector,
  Input,
  OnDestroy,
  OnInit, Optional,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { OPTION_PARENT_COMPONENT } from '../option/option/option.component';
import { Subject ,  of } from 'rxjs';
import { concatMap, debounceTime, takeUntil, tap } from 'rxjs/operators';
import { OptionModel } from '../option/option.model';
import { InputContainerComponent } from '../input-container/input-container.component';
import { SearchFunction, TypeaheadService } from './typeahead.service';
import { InputPositions } from '../input-positions';
import { FormFieldControl } from '../form-field-control';
import { AppTypeaheadDirective } from './typeahead.directive';

export const TYPEAHEAD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TypeaheadComponent), // tslint:disable-line
  multi: true,
};

@Component({
  selector: 'app-typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    TYPEAHEAD_VALUE_ACCESSOR,
    [{provide: OPTION_PARENT_COMPONENT, useExisting: TypeaheadComponent}]
  ]
})
export class TypeaheadComponent implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {
  @Input() public template: TemplateRef<any>;
  @Input() public key = '';
  @ViewChild('input') public input: ElementRef;
  public typeaheadControl = new FormControl();
  public results: OptionModel[] | null;
  public showDropdown = false;
  public onChange: any;
  public onTouched: any;
  public directive: FormFieldControl;

  private _value: any;
  private until: Subject<void> = new Subject<void>();
  private disabled = false;
  private search: SearchFunction;

  @Input()
  public set searchFunction(func) {
    this.search = func;
  }

  public get value() {
    return this._value;
  }

  public set value(data: OptionModel | null) {
    if (data !== null) {
      this._value = data.value;
      this.typeaheadControl.setValue(data.label, {emitEvent: false});
    } else {
      this._value = data;
    }
    this.results = null;
    this.showDropdown = false;
    this.onChange(this.value);
  }

  public get inputPaddingLeft() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  public get inputPaddingRight() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.RightTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  constructor(private cd: ChangeDetectorRef,
              private element: ElementRef,
              private service: TypeaheadService,
              @Optional() private container: InputContainerComponent,
              private injector: Injector) {
  }

  @HostListener('document:click', ['$event', '$event.target'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    this.cd.markForCheck();
    if (!targetElement || this.showDropdown !== true) {
      return;
    }

    const clickedInside = this.element.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.showDropdown = false;
      this.cd.markForCheck();
    }
  }

  public ngOnInit() {
    this.search = this.service.getSearchFunction(this.key);
    this.typeaheadControl.valueChanges
      .pipe(
        takeUntil(this.until),
        tap((v) => {
          if (this.value) {
            this.value = null;
          }
          this.container.placeholder.hidden = !!v;
          this.cd.markForCheck();
        }),
        debounceTime(300),
        concatMap((value) => value ? this.search(value) : of(null)),
      )
      .subscribe((res: OptionModel[] | null) => {
        this.results = res;
        this.showDropdown = true;
        this.cd.markForCheck();
      })
    ;
  }

  public ngAfterViewInit() {
    this.directive = this.injector.get(FormFieldControl, null);
    this.container.enableDefaultPlaceholderCheck = false;
    this.cd.detectChanges();
  }

  public resetValue() {
    this.value = {label: '', value: null};
    this.cd.markForCheck();
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public writeValue(obj: any): void {
    this._value = obj;
    if (this.value !== null) {
      this.search(obj)
        .subscribe((res) => {
          const label = res.find((item) => item.value === this.value).label;
          this.typeaheadControl.setValue(label, {emitEvent: false});
          this.container.placeholder.hidden = true;
        });
    }
    this.cd.detectChanges();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.complete();
  }
}
