import {
  AfterViewInit,
  Directive,
  DoCheck,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { FormFieldControl } from '../form-field-control';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { TypeaheadComponent } from './typeahead.component';
import { Subject } from 'rxjs';
import { InputPositions } from '../input-positions';

let nextUniqueId = 0;

@Directive({
  selector: 'app-typeahead[appInput]',
  providers: [{provide: FormFieldControl, useExisting: AppTypeaheadDirective}]
})
export class AppTypeaheadDirective implements FormFieldControl, DoCheck, OnDestroy {
  public readonly stateChange$: Subject<void> = new Subject<void>();
  public inputPosition: Array<'left' | 'right' | 'left-transparent' | 'right-transparent'> = [];
  public focused = false;

  protected _id: string;
  protected _uid = `app-input-${nextUniqueId++}`;
  protected _disabled = false;
  protected _required = false;
  protected _placeholder = '';
  protected _readonly = false;
  protected _inputIndent: number;

  public get inputIndent() {
    return this._inputIndent;
  }

  public set inputIndent(indent: number) {
    this._inputIndent = indent;
  }

  @HostBinding('class.has-error')
  public hasError = false;

  @HostBinding('class.input-right')
  public get inputRight() {
    return this.inputPosition.indexOf(InputPositions.Right) !== -1;
  }

  @HostBinding('class.input-left')
  public get inputLeft() {
    return this.inputPosition.indexOf(InputPositions.Left) !== -1;
  }

  @HostBinding('class.input-right-transparent')
  public get inputRightTransparent() {
    return this.inputPosition.indexOf(InputPositions.RightTransparent) !== -1;
  }

  @HostBinding('class.input-left-transparent')
  public get inputLeftTransparent() {
    return this.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1;
  }

  @HostListener('blur', ['false'])
  @HostListener('focus', ['true'])
  public focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && !this._readonly) {
      this.focused = isFocused;
      this.stateChange$.next();
    }
  }

  public get value() {
    return this.control && this.control.control ? this.control.control.value : null;
  }

  public set value(value: string) {
    if (value !== this.value) {
      this.control.control.setValue(value);
      this.stateChange$.next();
    }
  }

  @Input()
  public get disabled() {
    if (this.control && this.control.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }

  public set disabled(value: any) {
    this._disabled = !!value;
    this.typeahead.setDisabledState(!!value);
  }

  @Input()
  public get placeholder() {
    return this._placeholder;
  }

  public set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input()
  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(value: boolean) {
    this._required = !!value;
  }

  @Input()
  public get readonly(): boolean {
    return this._readonly;
  }

  public set readonly(value: boolean) {
    this._readonly = !!value;
  }

  public get element() {
    return this.elem.nativeElement;
  }

  public get control() {
    return this.ngControl;
  }

  public get parent() {
    return this._parentFormGroup || this._parentForm;
  }

  constructor(private typeahead: TypeaheadComponent,
              protected elem: ElementRef,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective) {
    this.id = this.id;
  }

  public ngDoCheck() {
    this.stateChange$.next();
  }

  public focus(): void {
    this.element.nativeElement.focus();
  }

  public ngOnDestroy() {
    this.stateChange$.complete();
  }
}
