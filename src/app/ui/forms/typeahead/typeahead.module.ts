import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeaheadComponent } from './typeahead.component';
import { AppTypeaheadDirective } from './typeahead.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { AppOptionsModule } from '../option/options.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AppOptionsModule,
  ],
  declarations: [
    TypeaheadComponent,
    AppTypeaheadDirective,
  ],
  exports: [
    TypeaheadComponent,
    AppTypeaheadDirective,
  ]
})
export class AppTypeaheadModule {

}
