import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortDirective } from './sort.directive';
import { SortHeaderComponent } from './sort-header.component';

const COMPONENTS = [
  SortDirective,
  SortHeaderComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [COMPONENTS],
  exports: [COMPONENTS],
})
export class SortModule {
}
