import { ChangeDetectorRef, Directive, EventEmitter, Input, Output } from '@angular/core';
import { SortHeaderComponent } from './sort-header.component';
import { Subject } from 'rxjs';
import { SortDirection } from './sort-direction';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

export interface AppSortable {
  /** The id of the column being sorted. */
  id: string;

  /** Starting sort direction. */
  start: 'asc' | 'desc';

  /** Whether to disable clearing the sorting state. */
  disableClear: boolean;
}

/** The current sort state. */
export interface Sort {
  /** The id of the column being sorted. */
  active: string;

  /** The sort direction. */
  direction: SortDirection;
}

@Directive({
  selector: '[appSort]',
  exportAs: 'appSort'
})
export class SortDirective {

  @Input('appSortActive') public active: string; //tslint:disable-line
  @Input('appSortStart') start: 'asc' | 'desc' = 'asc'; //tslint:disable-line
  public _stateChanges = new Subject<void>();
  /** Event emitted when the user changes either the active sort or sort direction. */
  @Output('appSortChange') readonly sortChange = new EventEmitter<Sort>(); //tslint:disable-line
  private sortables = new Map<string, SortHeaderComponent>();

  private _disabled = false;

  get disabled() {
    return this._disabled;
  }

  set disabled(value: any) {
    this._disabled = coerceBooleanProperty(value);
  }

  private _disableClear: boolean;

  @Input('appSortDisableClear')
  get disableClear() {
    return this._disableClear;
  }

  set disableClear(v: boolean) {
    this._disableClear = coerceBooleanProperty(v);
  }

  private _direction: SortDirection = '';

  get direction(): SortDirection {
    return this._direction;
  }

  @Input('appSortDirection')
  set direction(direction: SortDirection) {
    if (direction && direction !== 'asc' && direction !== 'desc') {
      throw new Error('Invalid sort direction');
    }
    this._direction = direction;
  }

  constructor(private cd: ChangeDetectorRef) {
  }

  public register(header: SortHeaderComponent) {
    if (this.sortables.has(header.id)) {
      throw new Error('Sortable id must be unique');
    }
    this.sortables.set(header.id, header);
  }

  public unregister(header: SortHeaderComponent) {
    this.sortables.delete(header.id);
  }

  public sort(sortable: SortHeaderComponent) {
    if (this.active !== sortable.id) {
      this.active = sortable.id;
      this.direction = sortable.start ? sortable.start : this.start;
    } else {
      this.direction = this.getNextSortDirection(sortable);
    }

    this.sortChange.next({active: this.active, direction: this.direction});
  }

  public setSortValue(sort, order) {
    this.active = sort;
    this.direction = order;
    this.sortables.get(sort).cd.detectChanges();
  }

  private getNextSortDirection(sortable: SortHeaderComponent): SortDirection {
    if (!sortable) {
      return '';
    }

    // Get the sort direction cycle with the potential sortable overrides.
    const disableClear = sortable.disableClear != null ? sortable.disableClear : this.disableClear;
    const sortDirectionCycle = getSortDirectionCycle(sortable.start || this.start, disableClear);

    // Get and return the next direction in the cycle
    let nextDirectionIndex = sortDirectionCycle.indexOf(this.direction) + 1;
    if (nextDirectionIndex >= sortDirectionCycle.length) {
      nextDirectionIndex = 0;
    }
    return sortDirectionCycle[nextDirectionIndex];
  }

}

function getSortDirectionCycle(start: 'asc' | 'desc',
                               disableClear: boolean): SortDirection[] {
  const sortOrder: SortDirection[] = ['asc', 'desc'];
  if (start === 'desc') {
    sortOrder.reverse();
  }
  if (!disableClear) {
    sortOrder.push('');
  }

  return sortOrder;
}
