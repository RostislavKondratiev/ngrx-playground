export class ApiConfig {
  public static base = '';
  public static local = 'http://localhost:3000';
  public static books = `${ApiConfig.local}/books`;
  public static magazines = `${ApiConfig.local}/magazines`;
  public static newspapers = `${ApiConfig.local}/newspapers`;
  public static posters = `${ApiConfig.local}/posters`;
}
