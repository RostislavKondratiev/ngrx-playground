import { Observable } from 'rxjs';

export class LocalStorage {

  public static setStorageItem(key, data) {
    if (data === null) {
      localStorage.removeItem(key);
      return;
    }
    localStorage.setItem(key, JSON.stringify(data));
  }

  public static getStorageItem(key) {
    return Observable.create((observer) => {
      const data = localStorage.getItem(key);
      if (data) {
        observer.next(JSON.parse(data));
      } else {
        observer.error('There is no storage item');
      }
      observer.complete();
    });
  }
}
